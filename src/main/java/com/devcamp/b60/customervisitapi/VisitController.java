package com.devcamp.b60.customervisitapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b60.customervisitapi.models.Customer;
import com.devcamp.b60.customervisitapi.models.Visit;

@RestController
@RequestMapping("/api")
public class VisitController {
    @CrossOrigin
    @GetMapping("/visits")

    public ArrayList<Visit> getVisits(){
        Customer customer1 = new Customer("Nguyễn Việt Phương");
        Customer customer2 = new Customer("Lê Thị Hiền");
        Customer customer3 = new Customer("Đặng Ngọc Viễn");

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Visit visit1 = new Visit(customer1, new java.sql.Date(0));
        Visit visit2 = new Visit(customer2, new java.sql.Date(0));
        Visit visit3 = new Visit(customer3, new java.sql.Date(0));
       

        System.out.println(visit1);
        System.out.println(visit2);
        System.out.println(visit3);

        ArrayList<Visit> arrListVisit = new ArrayList<Visit>();

        arrListVisit.add(visit1);
        arrListVisit.add(visit2);
        arrListVisit.add(visit3);

        return arrListVisit;
    }
}
